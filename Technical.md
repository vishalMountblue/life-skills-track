
# Message Queues

## Abstract
- Message Queues are basically a list of requests which are raised by clients.
- These requests are to be served when the destination program is connected.
- It provides asynchronous calling which allows an application to request a service and move on to other tasks.

## Introduction

- A message Queue provides temporary storage between the sender and receiver so that the sender can move to a new task without waiting for the destination program.
- Message Queue provides protection from service outages and failures.





## Case Study

### Pizza Shop

- Let us consider an example of a pizza shop where a customer visits a store and places an order.
- The cashier takes the order and gives a confirmation that the order will be prepared and served to you, till then have a seat, this can be referred to as an application-making request.
- Now, the cashier has not delivered the pizza but a verbal confirmation which lets the user do other tasks rather than standing in front of the cashier and waiting, the customer can browse his phone, talk to his friend, and other things, this can be referred as application raising a request and moving to other tasks.
- The order placed can be seen as a message queue where other orders are also present.
- When pizza making is done we remove it from our message queue and deliver the pizza.
- Now, consider there are several stores in the city, let's name them P1, P2, and P3. Assume P1 has a power cut, now P1 cannot update any orders but we need to have persistence in our data and therefore we require a database where the list of requests will be stored.
- To differentiate whether a server is dead, we have a notifier that checks each server at 15 - 30 seconds whether they are alive, this helps to know which shop is working properly.
- load balancing avoids duplicates to the same server, it avoids assigning the same order to a server who already has that order.
- All these things conclude the message queue.
- RabbitMq is a message broker which is used in between to receive and send messages.
## Conclusion
Message Queues take messages (data) from applications, store them till it is processed, and delete them when the consumer or entity which has raised the request retrieves the response.

## References
[1. What is a Message Queue and Where is it used?](https://www.youtube.com/watch?v=oUJbuFMyBDk&ab_channel=GauravSen)

[2. System Design — Message Queues](https://medium.com/must-know-computer-science/system-design-message-queues-245612428a22)



